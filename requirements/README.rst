Requirements
============

This folder contains supporting tools for the class. 

.. _`requirements.yml`: requirements.yml
`requirements.yml`_
    An  `Ansible`_ playbook used to install all necessary requirements on
    controller machine (``localhost``). It requires superuser privileges
    (*become* is used). Currently, it only supports Debian Stretch (and newer). ::

        ansible-playbook requirements.yml
    
.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
