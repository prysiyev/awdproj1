Setup environment
=================

.. _`Bouraci ucebna`: https://ict.fit.cvut.cz/web/classrooms/bouraci-ucebna/
.. _`git`: https://git-scm.com/   
.. _`KVM`: https://www.linux-kvm.org/page/Main_Page
.. _`libvirt`: http://libvirt.org/
.. _`QEMU`: http://www.qemu-project.org/   
.. _`Vagrant`: https://www.vagrantup.com/docs/getting-started/
.. _`Vagrant box`: https://www.vagrantup.com/docs/getting-started/boxes.html   
.. _`vm-2017-02-27`: https://gitlab.com/ctu-fit-bi-awd/images/blob/master/artifacts/vm-base-stretch/vm-2017-02-26.box

Class environment is based on `QEMU`_, `KVM`_ and `libvirt`_ which provide a
virtualization platform. `Vagrant`_ is used to orchestrate virtual machines
(VMs), i.e. to create, start and provision (network settings, proxy settings,
etc.) them.

Proxy settings
--------------

HTTP communication only is allowed from `Bouraci ucebna`_ and this communication
must go through HTTP proxy server (``http://proxy:3128``). It is a key
requirement to configure it correctly. Otherwise the workstation and VMs won't
be able to communicate with server in the Internet. 

Proxy configuration opens automatically upon login of user ``operator``. Provide
your credentials. The will be stored in ``~/.bashrc`` and will be used for git_
and Vagrant_. At the end of the class, shred the file. ::

    shred ~/.bashrc

All VMs will be provisioned with proxy configuration automatically.

Networking
----------

.. _`Vagrantfile`: Vagrantfile

Network setup is the same for each class. VMs are connected to three networks

*management*
  Network is used by Vagrant_ to manage VMs and connect to them using ``vagrant
  ssh VM`` command.

  Interface ``eth0`` in VM.

  IP addressing is handled automatically by DHCP.

*public*
  Network is shared by all VMs thus they can communicate with each other. IP
  addressing is handled by Vagrant_ (look at Vagrantfile_) 

  Controller machine is also the gateway and has IP address ``172.16.0.1``.

  Interface ``eth1`` in VM. 
  
*private*
  Network is prepared for custom use. Each VM has its own network and it is
  isolated from others

An example configuration of VMs may look as follows.

+-----------+-------------------+---------------+----------------+
| Host      | eth0 (management) | eth1 (public) | eth2 (private) |
+-----------+-------------------+---------------+----------------+
| server101 | dhcp              | 172.16.0.101  | down           |
+-----------+-------------------+---------------+----------------+
| server102 | dhcp              | 172.16.0.103  | down           |
+-----------+-------------------+---------------+----------------+
| server103 | dhcp              | 172.16.0.103  | down           |
+-----------+-------------------+---------------+----------------+

Users and accounts
------------------

All the VMs has preconfigured ``root`` user with password set. The user can
login using password over SSH (use ``vagrant ssh VM``). Credentials are:

* username: ``root``
* password: ``root``  

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
