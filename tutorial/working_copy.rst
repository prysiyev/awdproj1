Working copy
============

.. _git: https://git-scm.com/

This document describes how to obtain a working copy of the project (i.e.
materials for the current class). There are two strategies depending on your
preference. If you want to use the project once (in a class typically) go with
`Transient <#transient>`_ strategy. Your work is not preserved in this case! On the
other hand, if you want your work stored for later use go with `Persistent
<#persistent>`_ strategy.

Transient
---------

Open the terminal and download the project. There are two ways how to do it.

.. _archive: '../../../../repository/archive.tar.bz2?ref=master'
.. _repository: '../../../..'

#. Download an archive_ (replace ARCHIVE_URL) an unpack it. ::

     wget ARCHIVE_URL
     tar xjf archive.tar.bz2

#. Clone the repository_ (replace REPOSITORY_URL) using git_. ::

     git clone REPOSITORY_URL.git
  
Advantages:

* straightforward
* fast

Disadvantages:

* changes cannot be easily tracked and stored on a remote server for later use

Persistent
----------

.. _`gitlab`: https://about.gitlab.com/
.. _`gitlab.fit.cvut.cz`: https://gitlab.fit.cvut.cz

This case is bit complicated to setup but user can benefit from it later.
This approach uses git_ to track changes and gitlab_ as a remote repository
where changes are stored. 

Each student of FIT CTU has an account on `gitlab.fit.cvut.cz`_ and create new
projects there. 

Part 1 - get the project
~~~~~~~~~~~~~~~~~~~~~~~~

#. Open a terminal.
#. Create a directory for the project and initialize it as a git_ repository. ::
  
     mkdir project
     cd project
     git init
     
#. Add a remote `repository`_ (replace REPOSITORY_URL) and call it ``source``. ::

     git remote add source REPOSITORY_URL.git

#. Pull changes from ``source`` to the ``master`` branch of your working copy.
   ::

     git pull source master

#. Now you have all files from ``source`` in your working copy and you may start
   solving assigned tasks. If you want to track changes and push them later to
   remote repository continue reading Part 2 - create own remote.

Part 2 - create own remote
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _`CTU password`: http://intranet.cvut.cz/current-students/admin/password

In this part, we will create a project on gitlab_ server. The project is a
remote repository but it may also contain an issue tracker, CI integration etc.

#. Open a web browser and navigate to `gitlab.fit.cvut.cz`_.
#. Log in using your credentials (`CTU password`_).
#. Create new project.
#. Copy project URL (use HTTPs URL).
#. Go back to terminal to the project created in Part 1
#. Add a remote repository called ``origin`` (your project) using project URL
   (replace PROJECT_URL, it must end with ``.git``). ::

     git remote add origin PROJECT_URL
   
#. Push the changes to ``origin`` remote repository  and make git_ to track
   changes on ``master`` branch to ``origin/master``. ::

     git push -u origin master

#. Use git_ to track your changes in your working copy. Push them to ``origin``
   remote.

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
