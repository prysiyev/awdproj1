02 - Ansible
============

.. Here goes a summary of the class

.. _`Ansible`: https://ansible.com
.. _`RedHat, Inc.`: http://redhat.com  

Can you imagine to configure multiple same host by hand? Can you be sure that
all hosts are configured same way? Really?! How do you recover from failure? How
do you add new hosts? How many hosts can you handle and for how long? Can you
handle one more? Just this time?! 

Make you those questions uncomfortable or ever afraid? If you choose a proper
IT automation solution you won't have to worry (that much) any more. This class
will introduce `Ansible`_, IT automation solution by `RedHat, Inc.`_ 


Class contents
--------------

.. _`Lecture`: lecture/README.rst
.. _`Tutorial`: tutorial/README.rst

* `Lecture`_ (short study text, slides, example)
* `Tutorial`_

Requirements
------------

.. General knowledge requirements as well as technical requirements goes here.
   Specific requirements may be stated for lecture and tutorial as well.

Required knowledge
~~~~~~~~~~~~~~~~~~

* Basic shell knowledge
* Understanding of 

  * operating systems concepts (program, process, configuration, user, group,
    daemon)
  * networking concepts and protocols (encapsulation, IP network, addressing
    and routing; TCP - virtual circuit, multiplex; DNS; socket)

Technical requirements
~~~~~~~~~~~~~~~~~~~~~~

.. _`Ansible`: https://ansible.com
.. _`ansible-vagrant`: https://github.com/tomaskadlec/ansible-vagrant
.. _`git`: https://git-scm.com/
.. _`git-lfs`: https://github.com/git-lfs/git-lfs
.. _`kvm`: https://www.linux-kvm.org/page/Main_Page
.. _`libvirt`: https://libvirt.org/   
.. _`QEMU`: http://www.qemu-project.org/
.. _`Vagrant`: https://www.vagrantup.com/
.. _`vagrant-libvirt`: https://github.com/vagrant-libvirt/vagrant-libvirt

.. _`git submodule`: https://git-scm.com/book/en/v2/Git-Tools-Submodules

Examples as well as tutorial require:

* `Ansible`_ version 2.2 or newer
* `ansible-vagrant`_ (available as `git submodule`_)
* `git`_ and `git-lfs`_
* `QEMU`_, `libvirt`_ and optionally `kvm`_ for better performance
* `Vagrant`_ and `vagrant-libvirt`_

License
-------

.. _`Creative Commons Attribution-ShareAlike 4.0 International License`: http://creativecommons.org/licenses/by-sa/4.0/

.. figure:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
   :alt: Creative Commons License

   This work is licensed under a `Creative Commons Attribution-ShareAlike 4.0
   International License`_

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
