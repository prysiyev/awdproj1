Inventory and playbook
======================

.. |ansible.cfg| replace: ``ansible.cfg``
.. _`ansible.cfg`: ansible.cfg  
.. _`apt`: http://docs.ansible.com/ansible/apt_module.html
.. _`delegation`: http://docs.ansible.com/ansible/playbooks_delegation.html
.. |deploy.yml| replace: ``deploy.yml``
.. _`deploy.yml`: deploy.yml   
.. |hosts| replace: ``hosts``
.. _`hosts`: hosts   
.. _`ping`: http://docs.ansible.com/ansible/ping_module.html
.. _`service`: http://docs.ansible.com/ansible/service_module.html
   
One *play* of this example installs and starts a webserver. Another *play*
validates that hosts are up and web is working. Example show an advanced concept
of `delegation`_ when task is run for all host but in fact it is executed on
``localhost``.

#. |ansible.cfg|_ configuration file
#. |hosts| - inventory file
#. |deploy.yml|_ - playbook

   #. Play *Run web* uses apt_ and service_ modules to install and run the
      webserver.
   #. Play *Check webserver* uses ping_ to check availability of hosts and shell
      delegated to ``localhost`` to check whether web server is working.

Run the example
---------------

#. Firstly, the *vagrant-public* network must be created and started. Run
   following commands: ::

      export VIRSH_DEFAULT_CONNECT_URI=qemu:///system 
      virsh net-define libvirt-network.xml
      virsh net-start vagrant-public
      virsh net-autostart vagrant-public

#. Start VMs (leave the third for later): ::

     vagrant up --provider=libvirt server101 server102

#. Run the |deploy.yml|_ playbook. ::

     ansible-playbook deploy.yml

#. Start remaining VM: ::

     vagrant up --provider=libvirt server103

#. Run the |deploy.yml|_ playbook again and observe changes.

     ansible-playbook deploy.yml

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
