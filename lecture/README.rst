Ansible basics
==============

.. `Ansible`_: http://ansible.com

This lecture is a basic introduction to `Ansible`_, modern IT automation tool.
It describes basic concepts as *inventory*, *tasks*, *modules*, *plays* and
*playbooks*. Please refer to linked resources for detailed information.

.. _`examples`: examples/README.rst
.. _`slides`: https://docs.google.com/presentation/d/1tKqJEbfLVyaw2yXU-EAVLwWcYPFIPyq-LsueIJs-KyU/edit?usp=sharing

* `examples`_  
* `slides`_

IT automation
-------------

IT automation is not a necessity only for large scale deployments. It has its
important part disregarding scale. Automation allows reuse, teamwork,
versioning, fast deployment also recovery after a failure. `Ansible`_ is one
of the most popular tools available.

* It is easy to use.
* It does not require any agent on controlled hosts.
* It is idempotent.
* It is secure.

Ansible 
-------

.. _`YAML`: http://www.yaml.org/

`Ansible`_ makes use of (mostly) `YAML`_ syntax in its files. It does not matter
if file is a *playbook* or just definition of variables.

Configuration
~~~~~~~~~~~~~

.. _`Configuration file`: http://docs.ansible.com/ansible/intro_configuration.html#configuration-file

`Ansible`_ behaviour can be customized using configuration files and
environmental variables. Create a file named ``ansible.cfg`` in the root directory
of your project. Please refer to `Configuration file`_ page for reference.

File uses INI-style configuration files. Typical configuration is as follows: ::

    [defaults]
    # ssh key checking
    host_key_checking = false
    # inventory file
    hostfile = hosts
    # do not user cowsay 
    nocows = 1
    # merge hashes
    hash_behaviour = merge

Inventory
~~~~~~~~~

.. _`YAML format`: https://github.com/ansible/ansible/blob/devel/examples/hosts.yaml

Inventory is a database of managed hosts. `Ansible`_ supports INI-stye format and
`YAML format`_ (again since version 2.1). Hosts may be grouped together. Each group
can share variables. There is always group called ``all`` and all the hosts are
members of this group.

Example inventory may look as follows: ::

    server101    ansible_host=172.16.0.101
    server102    ansible_host=172.16.0.102
    server103    ansible_host=172.16.0.103

    [all:vars]
    ansible_user=root

`Ansible`_ can load variables also from another files. They are written in YAML
and are stored as shown bellow.

+--------+---------------------------+---------------------------------+
| Target | File                      | Directory                       |
+--------+---------------------------+---------------------------------+
| Group  | group_vars/GROUP_NAME.yml | group_vars/GROUP_NAME/file1.yml |
+--------+---------------------------+---------------------------------+
| Host   | host_vars/HOST_NAME.yml   | host_vars/HOST_NAME/file1.yml   |
+--------+---------------------------+---------------------------------+

Variables can be scalar or complex - lists (array) or dicts (recursive
associative array, or object-like). ::

    # ctu-fit-bi-awd/images/host_vars/classroom.yml

    # complex variable - dict
    distribution:
        name: debian
        version: testing
    # reference to another variable        
    installer: "{{debian_installer.testing}}"
    # simple
    configuration: files/debian_testing.preseed.cfg.j2
    disk_size: 6144
    # complex variable list of lists
    software:
        - [curl, git, less, procps, sudo, tmux, unrar-free, unzip, vim, wget, zip]
        - [apache2]

.. _`Inventory`: http://docs.ansible.com/ansible/intro_inventory.html

Please refer to `Inventory`_ page for further information.

Tasks
~~~~~

A task is a basic unit of `Ansible`_. It is a declaration of an action that
should be taken on managed hosts. Task can be executed in *ad-hoc* mode
(``ansible`` command) or tasks can be grouped into larger units called *plays* 
and *playbooks*.

.. _`plenty of modules available`: http://docs.ansible.com/ansible/modules_by_category.html
.. _`write a new one`: http://docs.ansible.com/ansible/dev_guide/developing_modules.html

A task is just a declaration. In fact, each task is a call to a *module*. Module
is the actual worker who does the job. There are `plenty of modules available`_ for
`Ansible`_ and it is not hard to `write a new one`_.

Just try `Ansible`_ in ad-hoc mode: ::

    operator@classroom $ ansible all -m ping

    server102 | SUCCESS => {
        "changed": false, 
        "ping": "pong"
    }
    server101 | SUCCESS => {
        "changed": false, 
        "ping": "pong"
    }
    server103 | UNREACHABLE! => {
        "changed": false, 
        "msg": "Failed to connect to the host via ssh: ssh: connect to host 172.16.0.103 port 22: No route to host\r\n", 
        "unreachable": true
    }

Plays and playbooks
~~~~~~~~~~~~~~~~~~~

.. _`module`: http://docs.ansible.com/ansible/modules_by_category.html

*Plays* are units that group related tasks together. ``name``, ``hosts``
that should be in that play are required always. A list of task may be present.
Each task is a call to a `module`_. ::

    -   name: "Run web"
        hosts: [webservers]
        tasks:
            -   apt:
                    package: apache2
                    state: latest
                when: ansible_distribution == 'Debian'

            -   service:
                    name: apache2
                    state: started
                    enabled: yes


Playbook is a file the play is stored in. Multiple plays can be together in one
playbook.

To run a playbook use: ::

    ansible-playbook playbook.yml

You can limit hosts in play by option ``--limit``, ``-l``.    

.. _`Best practices`: http://docs.ansible.com/ansible/playbooks_best_practices.html

Please read `Best practices`_ how to structure your `Ansible`_ based automation
project.

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
